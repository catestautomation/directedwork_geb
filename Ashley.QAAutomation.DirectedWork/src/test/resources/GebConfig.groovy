import GroovyAutomation.FunctionalLibrary

cacheDriver = false


environments {

    chrome {
        driver = {return FunctionalLibrary.GetDriver("chrome")}
    }

    firefox {
        driver ={return FunctionalLibrary.GetDriver("firefox")}
    }

    ie {
        driver ={return FunctionalLibrary.GetDriver("ie")}
    }

    phantomjs {
        driver ={return FunctionalLibrary.GetDriver("phantomjs")}
    }

}
