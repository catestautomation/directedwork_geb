package Pages

import GroovyAutomation.FunctionalLibrary
import geb.Page


class SetUpTimeOff25Page extends Page {
    static at ={
        heading.text()=="Directed Work :: Setup Off(25)"
    }

    static content ={
        heading {$("h2").first()}
        btnContainer {$("#box")}

        //field
        quantity {$("#txtQty0")}
        scrapCode {$("#ddlScrap0")}
        scrapQty {$("#txtScrapQty0")}

        okBtn {$("#btnOk")}

        informationOverlay{$("div.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-dialog-buttons")}
        overlayOKBtn {informationOverlay.find("span",text:"OK")}

        rFIDTagContainer {$("#dvEmptyTag")}
        tagValueField {$("#txtRFID0")}
        updateTagBtn {$("#btnupdTag")}
        scrapCodeErrorMsg {$("div#dvMessage").find("td").last()}

    }

    def setUpTimeOff25(String Qty,String ScrapQty,String ScrapCode){
        FunctionalLibrary.SetText(quantity, Qty)
        FunctionalLibrary.SetText(scrapQty, ScrapQty)
        scrapCode.click()
        scrapCode.find("option",text: contains(ScrapCode)).click()
        FunctionalLibrary.Click(okBtn)

    }


}
