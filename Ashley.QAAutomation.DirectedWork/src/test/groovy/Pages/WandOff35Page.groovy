package Pages

import GroovyAutomation.FunctionalLibrary
import geb.Page



class WandOff35Page extends Page {
    static at ={
        heading.text()=="Directed Work :: Wand Off(35)"
    }

    static content ={
        heading {$("h2").first()}
        wandOffContainer {$("#box")}

        //field
        quantity {$("#txtQty0")}
        scrapCode {$("#ddlScrap0")}
        palletComplete {$("select#ddlPLComplete0")}

        okBtn {$("#btnOk")}

        informationOverlay{$("div.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-dialog-buttons")}
        overlayOKBtn {informationOverlay.find("span",text:"OK")}
        overlayCancelBtn {$("span",text: "Cancel")}
        scrapCodeErrorMsg {$("div#dvMessage").find("td").last()}

    }

    def wandOff35(String Qty,String ScrapCode, String PalletComplete){
//        String scrapcode = ScrapCode.padLeft(3)
//        String palletcomplete = PalletComplete.padLeft(4)
        FunctionalLibrary.SetText(quantity, Qty)
        scrapCode.click()
        scrapCode.find("option",text: contains(ScrapCode)).click()
        palletComplete.click()
        palletComplete.find("option",text: contains(PalletComplete)).click()

        FunctionalLibrary.Click(okBtn)
    }
}
