package Pages

import GroovyAutomation.FunctionalLibrary
import geb.Page


class OperatorWorkCenterPage extends Page {
    static at = {
        heading.text().contains("LIST OF WORKCENTER FOR THE ZONE")

    }
    static content = {
        //Text
        heading { $("#hdrWrokCenter") }

        //Container
        operatorWorkCenter {$("input.ZoneButton1")}
        groupContainer {$("#responsive") }
    }

    def operatorWorkCenterSelection(String WorkCenter) {
        for(int i=0;i<operatorWorkCenter.size();i++){
            String WorkCenterName = operatorWorkCenter.getAt(i).attr("value")
            if(WorkCenter.toUpperCase()==WorkCenterName.toUpperCase()) {
                FunctionalLibrary.Click(operatorWorkCenter.getAt(i))
                break;
            }

        }
    }

}
