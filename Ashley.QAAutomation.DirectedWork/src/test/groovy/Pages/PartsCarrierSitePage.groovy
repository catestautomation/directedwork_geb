package Pages

import GroovyAutomation.FunctionalLibrary
import geb.Page


class PartsCarrierSitePage extends Page {

        static at = {
            heading.text().contains("LIST OF SITES")

        }
        static content = {
            //Text
            heading { $("#hdrWrokCenter") }

            //Container
            partsCarrierSite { $("input.SiteButton") }
        }

        def partsCarrierSiteSelection(String Site) {

            for (int i = 0; i < partsCarrierSite.size(); i++) {
                String siteName = partsCarrierSite.getAt(i).attr("value").replace(" ", "")
                if (Site == siteName) {
                    FunctionalLibrary.Click(partsCarrierSite.getAt(i))
                    break;
                }
            }
        }
}
