package Pages

import GroovyAutomation.FunctionalLibrary
import geb.Page


class OperatorGroupsPage extends Page {
    static at = {
        groupContainer.present

    }
    static content = {
        //Text
        heading { $("#hdrWrokCenter") }

        //Container
        operatorWorkCenter {$("input.ZoneButton1")}

        groupSelect {$("select#ulGrpLst") }
        groupSelectOkBtn {$("#saveEdit")}
        groupContainer {$("#responsive") }
    }
    def operatorGroupSelection(String Group){
        groupSelect.click()
        groupSelect.find("option",text: contains(Group) ).click()
        FunctionalLibrary.Click(groupSelectOkBtn)
    }

}
