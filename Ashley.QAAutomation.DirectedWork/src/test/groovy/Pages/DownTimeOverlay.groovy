package Pages

import GroovyAutomation.FunctionalLibrary
import geb.Page

class DownTimeOverlay extends Page{
    static at = {
        downTimeContainer.present
    }
    static content = {
        downTimeContainer { $("#DownTimeoffid") }
        downTimeCode { $("#ddlDownTimeCode") }
        downTimeCodeOKBtn { $ "#btnDownok" }
        downTimeComments { $("#txtDownTimeComments") }
    }

    def downTimeReason (String Code){
        downTimeCode.click()
        downTimeCode.find("option",text: contains(Code)).click()
        FunctionalLibrary.Click(downTimeCodeOKBtn)
    }
}
