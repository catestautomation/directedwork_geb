package Pages

import geb.Page

class HeaderPage extends Page {

    static at = {
        AshleyLogo.present
    }

    static content = {
        AshleyLogo { $("div.logo").find("img") }
        logOutBtn {$("img.logOutbtn")}
        homeBtn {$("img.homeBtn")}


    }
}
