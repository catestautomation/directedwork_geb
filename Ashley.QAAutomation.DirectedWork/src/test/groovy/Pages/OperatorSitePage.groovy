package Pages

import GroovyAutomation.FunctionalLibrary
import geb.Page



class OperatorSitePage extends Page {
    static at = {
        heading.text().contains("LIST OF SITES")

    }
    static content = {
        //Text
        heading { $("#hdrWrokCenter") }

        //Container
        operatorSite { $("input.SiteButton") }
    }

    def operatorSiteSelection(String Site) {

        for (int i = 0; i < operatorSite.size(); i++) {
            String siteName = operatorSite.getAt(i).attr("value").replace(" ", "")
            if (Site == siteName) {
                FunctionalLibrary.Click(operatorSite.getAt(i))
                break;
            }
        }
    }
}