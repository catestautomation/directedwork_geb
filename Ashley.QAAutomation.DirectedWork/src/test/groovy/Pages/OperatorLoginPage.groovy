package Pages

import geb.Page

public class OperatorLoginPage extends Page{
    static at =  {
        AshleyLogo.present
    }

    static content = {
        AshleyLogo {$("div.logo").find("img")}

        //Container
        loginScreen {$("#loginScreen")}

        //Fileds
        userName {$("#UserName")}
        password {$("#Password")}

        //Button
        loginBtn{$("input.loginBtn")}


    }
}
