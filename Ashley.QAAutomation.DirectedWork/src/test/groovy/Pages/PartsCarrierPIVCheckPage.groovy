package Pages

import GroovyAutomation.FunctionalLibrary
import geb.Page

class PartsCarrierPIVCheckPage extends Page {

    static at = {
        pIVCheck.present
    }

    static  content ={
        pIVCheck {$("#aPIVcheckcomplete")}
        assemblyLinePC {$("#AssemblyLinePC")}
        fabricationPC {$("#FabricationPC")}

    }

    def PIVCheck(String input){
        FunctionalLibrary.Click(pIVCheck)
        if(input.contains("Assembly")){
            FunctionalLibrary.Click(assemblyLinePC)
        }
        else{
            FunctionalLibrary.Click(fabricationPC)
        }
    }



}
