package Pages

import geb.Page

/**
 * Created by Saranya.S on 1/13/2017.
 */
class PartsCarrierTaskListPage extends Page {

    static at ={
        heading.present
    }

    static content ={
        heading {$("#pTask")}
    }
}
