package Pages

import GroovyAutomation.FunctionalLibrary
import geb.Page



class OperatorZonePage extends Page{

    static at = {
        heading.text().contains("LIST OF ZONES FOR THE SITE")

    }
    static content = {
        //Text
        heading { $("#hdrWrokCenter") }

        //Container
        operatorZone {$("input.ZoneButton")}
    }

    def operatorZoneSelection(String Zone) {
        for(int i=0;i<operatorZone.size();i++){
            String zoneName = operatorZone.getAt(i).attr("value")
            if(Zone.toUpperCase()==zoneName.toUpperCase()){
                FunctionalLibrary.Click(operatorZone.getAt(i))
                break;
            }

        }
    }
}
