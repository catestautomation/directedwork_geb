package Pages

import GroovyAutomation.FunctionalLibrary
import geb.Page

class DownnTimeOff55Page extends Page {
    static at = {
        downTimeOkbtn.present
    }

    static content = {

        btnContainer { $("#box") }
        downTimeOkbtn {$("#btnOk")}
        downTimeCode{$("#ddlDownTimeCode")}

    }

}
