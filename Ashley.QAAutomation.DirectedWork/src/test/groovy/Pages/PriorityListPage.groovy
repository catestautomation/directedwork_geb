package Pages

import GroovyAutomation.FunctionalLibrary
import geb.Page

import java.util.function.Function


class PriorityListPage extends Page {

    static at = {
        heading.present
    }
    static content = {
        //Text
        heading { $("#hdrWrokCenter") }

        //Fileds
        enterMO { $("#txtMOFilter") }

        //Buttons
        moSearchBtn { $("span#basic-addon2") }
        setUpTImeOn {$("#btnWndSetUp")}
        setUpTimeOff {$("#btnWndSetupOff")}
        downTimeOn {$("#btnDownTimeOn")}
        downTimeOff {$("#btnDownTimeOff")}
        requestWork {$("#btnReqWrk")}
        wandOff {$("#btnWndOffLbr")}
        wandOn {$("#btnWndOnLbr")}
        addToGrouop {$("#btnAddGroup60")}
        removeFromGroup {$("#btnRemoveGroup65")}
        addRowForManualWandOn{$("#btnAddRow")}
        serialScan {$("#txtSerial")}
        serialScanOkBtn {$("#btnupdTag")}
        serialScanCancelBtn {$("#btnCancelTag")}
        lowerGrid {$("#contenttablebox1")}
        noDataMessage {lowerGrid.$("span",text:"No data to display")}

        groupBlink {$("#spnGrp")}
        upperGridFilteredRow { $("#row0box") }
        lowerGridRow {$("#row0box1").children().first()}

        // Add to Group Container
        addToGroupContainer {$("#AddGroupid")}
        addEmpNumber {$("#txtEmpname")}
        infoAlert {$("span",text:"OK")}
        removeEmpNumber {$("#txtEmpname")}
        okBtn {$("#btnAddGroupok")}
        cancelBtn {$("#btnAddGroupCancel")}

        serialScanPopUp {$("#dvCurrentScanPopup")}
        infoAlertForRemoveEmpFromGroup {$("#dvMessage").find("td").text()}
        rowParent {$("div#row0box1")}
        rowValue {$("div.jqx-grid-cell.jqx-item").last().previous()}

    }

    def searchMO(String MO) {
        FunctionalLibrary.SetText(enterMO, MO)
        FunctionalLibrary.Click(moSearchBtn)
        upperGridFilteredRow.find("div",text:MO).first().click()
        FunctionalLibrary.WaitForPageLoad(10)
    }

    def addEmployeeToGroup(String EmpNo){
        FunctionalLibrary.SetText(addEmpNumber, EmpNo)
        FunctionalLibrary.Click(okBtn)
        FunctionalLibrary.Click(infoAlert)

    }

    def removeEmpFromGroup(String EmpNo){
        FunctionalLibrary.SetText(removeEmpNumber, EmpNo)
        FunctionalLibrary.Click(okBtn)
    }

    def scanSerialNo(String Serial){
        FunctionalLibrary.SetText(serialScan,Serial)
        FunctionalLibrary.Click(serialScanOkBtn)

    }




}

