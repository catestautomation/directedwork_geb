package Pages

import GroovyAutomation.FunctionalLibrary
import geb.Page

class RequestWorkPage extends Page {
    static at = {
        heading.present
    }
    static content = {
        //Text
        heading { $("#hdrWrokCenter") }
        workRequestCollection {$("#workRequest").find("ul").find("li").findAll() }
        taskList {$("#taskListStatus").find("table").first()}

        taskListRows {taskList.find("tr").findAll()}
        taskListStatus {taskList.find("tr").last().find("td").first()}
        workRequest {$("input.operatorBtn")}

        assemblyTaskValue {$("txtPartNumber")}
        btnOk {$("#btnAssemblyOk")}
        btnCancel {$("#btnAssemblyCancel")}


    }

    def workRequestSelection(String Request){
        for(int i=0;i<workRequest.size();i++){
            String WorkReq = workRequest.getAt(i).attr("value")
            if(Request.toUpperCase()==WorkReq.toUpperCase()) {
                FunctionalLibrary.Click(workRequest.getAt(i))
                break;
            }

        }
    }

    def lastAddedTask(String addedTask){
       def tr= taskList.find("td",text:" + addedTask + ").find("tr")

    }


}