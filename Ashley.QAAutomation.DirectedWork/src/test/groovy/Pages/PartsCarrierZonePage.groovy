package Pages

import GroovyAutomation.FunctionalLibrary
import geb.Page

/**
 * Created by Saranya.S on 1/13/2017.
 */
class PartsCarrierZonePage extends Page{

    static at = {
        heading.text().contains("LIST OF ZONES FOR THE SITE")

    }
    static content = {
        //Text
        heading { $("#hdrWrokCenter") }

        //Container
        partsCarrierZone {$("input.ZoneButton")}
    }

    def partsCarrierZoneSelection(String Zone) {
        for(int i=0;i<partsCarrierZone.size();i++){
            String zoneName = partsCarrierZone.getAt(i).attr("value")
            if(Zone.toUpperCase()==zoneName.toUpperCase()){
                FunctionalLibrary.Click(partsCarrierZone.getAt(i))
                break;
            }

        }
    }
}
