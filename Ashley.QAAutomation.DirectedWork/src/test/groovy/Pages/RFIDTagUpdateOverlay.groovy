package Pages

import geb.Page

class RFIDTagUpdateOverlay extends Page {
    static at = {
        tagValueField.present
    }
    static content = {


        tagValueField { $("#txtRFID0") }
        updateTagBtn { $("#btnupdTag") }
    }
}
