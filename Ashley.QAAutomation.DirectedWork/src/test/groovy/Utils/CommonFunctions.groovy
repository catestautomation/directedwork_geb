package Utils

import GroovyAutomation.FunctionalLibrary
import Pages.DownTimeOverlay
import Pages.DownnTimeOff55Page
import Pages.HeaderPage
import Pages.OperatorGroupsPage
import Pages.OperatorLoginPage
import Pages.OperatorSitePage
import Pages.OperatorWorkCenterPage
import Pages.OperatorZonePage
import Pages.PartsCarrierPIVCheckPage
import Pages.PartsCarrierSitePage
import Pages.PartsCarrierZonePage
import Pages.RFIDTagUpdateOverlay
import geb.Browser
import geb.spock.GebSpec

final class CommonFunctions extends GebSpec {

    public static Browser currentBrowser = null;

    def static void login(String userName, String password) {
        FunctionalLibrary.SetText(currentBrowser.at(OperatorLoginPage).userName,userName)
        FunctionalLibrary.SetText(currentBrowser.at(OperatorLoginPage).password,password)
        FunctionalLibrary.Click(currentBrowser.at(OperatorLoginPage).loginBtn,30)
    }

    def static void operatorSiteZoneWorkCenterGroupSelection(String Site, String Zone, String WorkCenter, String Group){
        currentBrowser.at(OperatorSitePage).operatorSiteSelection(Site)
        currentBrowser.at(OperatorZonePage).operatorZoneSelection(Zone)
        currentBrowser.at(OperatorWorkCenterPage).operatorWorkCenterSelection(WorkCenter)
        currentBrowser.at(OperatorGroupsPage).operatorGroupSelection(Group)
    }

    def static void updateRFIDTag(String RFID){
        FunctionalLibrary.SetText(currentBrowser.at(RFIDTagUpdateOverlay).tagValueField, RFID)
        FunctionalLibrary.Click(currentBrowser.at(RFIDTagUpdateOverlay).updateTagBtn)
        FunctionalLibrary.WaitForPageLoad(20)
    }

    def static void downTimeOff(){
        FunctionalLibrary.Click(currentBrowser.at(DownnTimeOff55Page).downTimeOkbtn)
    }

    def static void downTimeReason(String Code){
        currentBrowser.at(DownTimeOverlay).downTimeReason(Code)

    }

    def static void logOut(){
        FunctionalLibrary.Click(currentBrowser.at(HeaderPage).logOutBtn)
    }

    def static void partsCarrierSiteZonePIVCheck(String Site, String Zone,String PIVCheck){
        currentBrowser.at(PartsCarrierSitePage).partsCarrierSiteSelection(Site)
        currentBrowser.at(PartsCarrierZonePage).partsCarrierZoneSelection(Zone)
        currentBrowser.at(PartsCarrierPIVCheckPage).PIVCheck(PIVCheck)

    }



}