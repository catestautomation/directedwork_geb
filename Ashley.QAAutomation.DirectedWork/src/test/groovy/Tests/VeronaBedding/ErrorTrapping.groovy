package Tests.VeronaBedding
import GroovyAutomation.CSVDataSource
import GroovyAutomation.FunctionalLibrary
import Pages.DownTimeOverlay
import Pages.DownnTimeOff55Page
import Pages.PriorityListPage
import Pages.WandOff35Page
import Utils.CommonFunctions
import geb.driver.CachingDriverFactory
import geb.spock.GebReportingSpec
import spock.lang.Unroll

class ErrorTrapping extends GebReportingSpec{
    void resetBrowser() {
        def driver = browser.driver
        super.resetBrowser()
        driver.quit()
        CachingDriverFactory.clearCache()
    }

    def setup() {
        CommonFunctions.currentBrowser = getBrowser()
        FunctionalLibrary.currentBrowser = getBrowser()
    }

    @Unroll("116887 : 'Browser: #envBrowser'-'#envURL'")
    def "01 - 116887: To Verify that respective off screen should display while On transaction is performed which is already On"(){
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 2, Select Begin scan button
        at PriorityListPage
        FunctionalLibrary.Click(requestWork)

        // 3, Scan a serial number
        when:
        scanSerialNo(serialNumber)
        //Need to verify if the MO, OpSeq & Item# is displayed in the popup

        FunctionalLibrary.Click(serialScanCancelBtn)

        // 4, Select 50
        FunctionalLibrary.Click(downTimeOn)

        then:
        at WandOff35Page
        FunctionalLibrary.Click(okBtn)

        // 5, Select ok
        FunctionalLibrary.Click(overlayOKBtn)

        // 6, Select Downtimecode
        when:
        at DownTimeOverlay
        downTimeReason(reason)

        at PriorityListPage
        // 7, Select 55 button
        FunctionalLibrary.Click(downTimeOff)

        // 8, Select ok
        then:
        at DownnTimeOff55Page
        FunctionalLibrary.Click(downTimeOkbtn)

        where: "Getting Test Data from CSV"
        [username,password,site,zone,workcenter,group,serialNumber] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\116887.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")

    }

    @Unroll("116888 : 'Browser: #envBrowser'-'#envURL'")
    def "02 - 116888: To Verify that respective off screen should display while multiple On transaction is performed which is already On"(){
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 2, Select Begin scan button
        at PriorityListPage
        FunctionalLibrary.Click(requestWork)

        // 3, Scan a serial number
        when:
        scanSerialNo(serialNumber)
        //Need to verify if the MO, OpSeq & Item# is displayed in the popup

        // 4, Scan another serial number
        scanSerialNo(serialNumber1)
        //Need to verify if the MO, OpSeq & Item# is displayed in the popup

        FunctionalLibrary.Click(serialScanCancelBtn)

        // 5, Select 50
        FunctionalLibrary.Click(downTimeOn)

        then:
        at WandOff35Page
        FunctionalLibrary.Click(okBtn)

        // 6, Select ok
        FunctionalLibrary.Click(overlayOKBtn)

        // 7, Select Downtimecode
        when:
        at DownTimeOverlay
        downTimeReason(reason)
        assert lowerGridRow.css("background-color")== "rgbs(255, 0, 0, 1)"

        at PriorityListPage
        // 8, Select 55 button
        FunctionalLibrary.Click(downTimeOff)

        // 9, Select ok
        then:
        at DownnTimeOff55Page
        FunctionalLibrary.Click(downTimeOkbtn)

        where: "Getting Test Data from CSV"
        [username,password,site,zone,workcenter,group,serialNumber,serialNumber1,reason] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\116887.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")

    }

    @Unroll("116889 : 'Browser: #envBrowser'-'#envURL'")
    def "03 - 116889: To Verify that respective off screen should display while On 50 done followed by scanning"(){
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 2, Select Begin scan button
        at PriorityListPage
        FunctionalLibrary.Click(overlayOKBtn)

        // 3, Select Downtimecode
        when:
        at DownTimeOverlay
        downTimeReason(reason)

        then:
        assert lowerGridRow.css("background-color")== "rgbs(255, 0, 0, 1)"

        // 4, Select Begin scan button

        at PriorityListPage
        FunctionalLibrary.Click(requestWork)

        // 5, select ok
        driver.switchTo().alert().accept()

        // 6, Select Downtimecode
        when:
        at DownnTimeOff55Page
        downTimeReason(reason)

        // 7, Scan a serial number
        at PriorityListPage
        scanSerialNo(serialNumber)
        //Need to verify if the MO, OpSeq & Item# is displayed in the popup

        // 8, Scan another serial number
        scanSerialNo(serialNumbe1)
        //Need to verify if the MO, OpSeq & Item# is displayed in the popup

        then:
        FunctionalLibrary.Click(serialScanCancelBtn)

        // 9, Select 35 button
        FunctionalLibrary.Click(wandOff)

        at WandOff35Page
        // 10, Select ok
        FunctionalLibrary.Click(okBtn)

        FunctionalLibrary.Click(overlayOKBtn)

        where: "Getting Test Data from CSV"
        [username,password,site,zone,workcenter,group,serialNumber,serialNumber1,reason] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\116887.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")

    }

}

