package Tests.VeronaBedding

import GroovyAutomation.CSVDataSource
import GroovyAutomation.FunctionalLibrary
import Pages.PriorityListPage
import Pages.WandOff35Page
import Utils.CommonFunctions
import geb.driver.CachingDriverFactory
import geb.spock.GebReportingSpec

import spock.lang.Unroll

class DCSTranscation extends GebReportingSpec {
    void resetBrowser() {
        def driver = browser.driver
        super.resetBrowser()
        driver.quit()
        CachingDriverFactory.clearCache()
    }

    def setup() {
        CommonFunctions.currentBrowser = getBrowser()
        FunctionalLibrary.currentBrowser = getBrowser()
    }

//    @Unroll("116532 : 'Browser: #envBrowser' - '#envURL'")
//    def "01 - 116532: Check 35 screen appears while trying to perform 35 after successful scanning"() {
//        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
//        // 1.2, Enter valid @Operatorusername in Username field
//        // 1.3, Enter valid @Operatorpassword in Password field
//        // 1.4, Click on "Login" button
//
//        CommonFunctions.login(username, password)
//
//        // 1.5, Select @OperatorSite
//        // 1.6, Select @OperatorZone
//        // 1.7, Select @OperatorWorkCenter
//        // 1.8, Select @Group and click on 'OK' button
//        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)
//
//        // 2, Select Begin scanning button
//        at PriorityListPage
//        FunctionalLibrary.Click(requestWork)
//
//        // 3, Scan/Enter a serial number
//        when:
//        scanSerialNo(serialNumber)
//
//        // 4, Verify the Large Pop up
//        then:
////        assert serialScanPopUp.css("display") == "block"
//
//        // 5, Verify the Lower Grid
//
//        assert rowValue.attr("title").contains(serialNumber)
//
//        // 6, Select Wand off button
//        FunctionalLibrary.Click(wandOff)
//
//        // 7, Verify the Wand off screen
//
//        at WandOff35Page
//        assert quantity.attr("value") == "1"
//
//        FunctionalLibrary.Click(okBtn)
//
//
//        where: "Getting Test Data from CSV"
//        [username,password,site,zone,workcenter,group,serialNumber] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\116532.csv")
//        envBrowser = System.getProperty("geb.env")
//        envURL = System.getProperty("geb.build.baseUrl")
//
//
//    }

    @Unroll("116534 : 'Browser: #envBrowser' - '#envURL'")
    def "01 - 116534: Check that 55 screen appears when scanning an Mo with 50 done"() {
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 2, Select 50
        at PriorityListPage
        FunctionalLibrary.Click(downTimeOn)

        CommonFunctions.downTimeReason(code)

        at PriorityListPage
        assert lowerGridRow.css("background-color")== "rgba(255, 0, 0, 1)"


        // 3, Select Begin scan button

        when:
        at PriorityListPage
        FunctionalLibrary.Click(requestWork)

        driver.switchTo().alert().accept()

        // 4, select ok in the 55 screen
        // 5, Verify the screen
        CommonFunctions.downTimeOff()

        // 6, Scan a serial number

        at PriorityListPage
        scanSerialNo(serialNumber)
        assert lowerGridRow.css("background-color") != "rgba(255, 0, 0, 1)"

        then:

      //  def fg = serialScanPopUp.css("display")
        //  assert serialScanPopUp.css("display") == "block"

        // 7, Verify the lower Grid

        FunctionalLibrary.Click(serialScanCancelBtn)

        assert rowValue.attr("title").contains(serialNumber)

        // 6, Select Wand off button
        FunctionalLibrary.Click(wandOff)

        // 7, Verify the Wand off screen

        at WandOff35Page
        assert quantity.attr("value") == "1"

        FunctionalLibrary.Click(okBtn)

        FunctionalLibrary.Click(overlayOKBtn)

        where: "Getting Test Data from CSV"
        [username,password,site,zone,workcenter,group,code,serialNumber] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\116534.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")


    }

//    @Unroll("116534 : 'Browser: #envBrowser' - '#envURL'")
//    def "03 - 116534: Check that when time reaches should show Time for DCS transaction pop occurs followed by dcs"() {
//        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
//        // 1.2, Enter valid @Operatorusername in Username field
//        // 1.3, Enter valid @Operatorpassword in Password field
//        // 1.4, Click on "Login" button
//
//        CommonFunctions.login(username, password)
//
//        // 1.5, Select @OperatorSite
//        // 1.6, Select @OperatorZone
//        // 1.7, Select @OperatorWorkCenter
//        // 1.8, Select @Group and click on 'OK' button
//        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)
//
//        // 2, Select Begin scan button
//
//        // 3, Scan a @serial number
//
//        // 4, wait for time to reach dcs time out
//
//        // 5,Select ok
//
//        // 6, Verify the 35 screen
//
//        // 7, Edit the @QTY and select ok
//
//        // 8, Verify the qty in Discrepencay report
//
//
//
//
//
//
//
//
//    }

    @Unroll("116886 : 'Browser: #envBrowser' - '#envURL'")
    def "02 - 116886: Check that Transaction is retained when system shuts down/power off/network outrage"() {

        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 2, Select Begin scan button

        at PriorityListPage
        FunctionalLibrary.Click(requestWork)

        // 3, Scan a @serial number
        when:
        scanSerialNo(serialNumber)

        // 4, Introduce an interrupt by shutting down system or unplug network cable
        go "http://stagemfg.ashleyfurniture.com/Ashley.Manufacturing.Directedwork.Operators/Ashley/login.html"

        // 5, Login to the same group

        CommonFunctions.login(username, password)

        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        then:
        at PriorityListPage
        assert rowValue.attr("title").contains(serialNumber)

        FunctionalLibrary.Click(wandOff)

        at WandOff35Page
        assert quantity.attr("value") == "1"

        FunctionalLibrary.Click(okBtn)

        FunctionalLibrary.Click(overlayOKBtn)

        where: "Getting Test Data from CSV"
        [username,password,site,zone,workcenter,group,serialNumber] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\116886.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")


    }

    @Unroll("116891 : 'Browser: #envBrowser' - '#envURL'")
    def "03 - 116891: Check that it allows to perform 50 followed by a 50 again"() {
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 2, Select 50 button
        when:
        at PriorityListPage
        FunctionalLibrary.Click(downTimeOn)


        CommonFunctions.downTimeReason(code)

        at PriorityListPage
        FunctionalLibrary.Click(downTimeOff)

        CommonFunctions.downTimeOff()

        // 3, Again select 50
        then:
        at PriorityListPage
        FunctionalLibrary.Click(downTimeOn)

        CommonFunctions.downTimeReason(code)

        at PriorityListPage
        FunctionalLibrary.Click(downTimeOff)

        CommonFunctions.downTimeOff()

        where: "Getting Test Data from CSV"
        [username,password,site,zone,workcenter,group,code] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\116891.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")
    }

    @Unroll("116892 : 'Browser: #envBrowser' - '#envURL'")
    def "04 - 116892: Check that Cancel button should cancel the scanning process"() {
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 2, Select Begin scan button
        when:
        at PriorityListPage
        FunctionalLibrary.Click(requestWork)

        // 3, Select Cancel button
        FunctionalLibrary.Click(serialScanCancelBtn)

        then:
        at PriorityListPage
        assert noDataMessage.present

        where: "Getting Test Data from CSV"
        [username,password,site,zone,workcenter,group] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\116892.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")


    }

    @Unroll("116885 : 'Browser: #envBrowser' - '#envURL'")
    def "05 - 116885:Check multiple wand on for same order is allowed"() {
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 2, Select Begin scan button
        when:
        at PriorityListPage
        FunctionalLibrary.Click(requestWork)

        // 3, Scan Serial number
        scanSerialNo(serialNumber)

        scanSerialNo(serialNumber1)

        FunctionalLibrary.Click(serialScanCancelBtn)


        then:
        FunctionalLibrary.Click(wandOff)

        // 4, Verify the Wand off screen

        at WandOff35Page
        assert quantity.attr("value") == "2"

        FunctionalLibrary.Click(okBtn)

        FunctionalLibrary.Click(overlayOKBtn)


        where: "Getting Test Data from CSV"
        [username,password,site,zone,workcenter,group,serialNumber,serialNumber1] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\116885.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")

    }

//    @Unroll("116893 : 'Browser: #envBrowser' - '#envURL'")
//    def "08 - 116893: Check that when a invalid serial scan encounter other valid scan should be processed successfully"(){
//
//       // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
//       // 1.2, Enter valid @Operatorusername in Username field
//       // 1.3, Enter valid @Operatorpassword in Password field
//       // 1.4, Click on "Login" button
//        CommonFunctions.login(username, password)
//
//       // 1.5, Select @OperatorSite
//       // 1.6, Select @OperatorZone
//       // 1.7, Select @OperatorWorkCenter
//       // 1.8, Select @Group and click on 'OK' button
//        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)
//
//       // 2, Select Begin scan button
//        when:
//        at PriorityListPage
//        FunctionalLibrary.Click(requestWork)
//
//       // 3, Scan an serial number
//        scanSerialNo(serial)
//
//       // 4, Scan an invalid serial number
//        scanSerialNo(serial1)
//
//       // 5, Verify the Alert pop up
//
//        FunctionalLibrary.Click(serialScanCancelBtn)
//       // 6, Verify the lower grid (should show the scanned serial)
//        then:
//        assert lowerGridRowSerial.attr("title").contains(serial)
//
//       // 7, Select 35 Wand Off
//
//        FunctionalLibrary.Click(wandOff)
//
//        at WandOff35Page
//
//        FunctionalLibrary.Click(okBtn)
//        FunctionalLibrary.Click(overlayOKBtn)
//
//        where: "Getting Test Data from CSV"
//        [username,password,site,zone,workcenter,group,serial,serial1] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\116893.csv")
//        envBrowser = System.getProperty("geb.env")
//        envURL = System.getProperty("geb.build.baseUrl")
//
//    }

    @Unroll("117235 : 'Browser: #envBrowser' - '#envURL'")
    def "06 - 117235: Check multiple wand off for same order is allowed"(){
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button
        CommonFunctions.login(username, password)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 2, Select Begin scan button
        when:
        at PriorityListPage
        FunctionalLibrary.Click(requestWork)

        // 3, Scan an serial number
        scanSerialNo(serial)

        // 4, Scan an serial number from the same order
        scanSerialNo(serial1)
        FunctionalLibrary.Click(serialScanCancelBtn)

        // 5, Select 35 Wand Off
        then:
        FunctionalLibrary.Click(wandOff)

        at WandOff35Page

       // 6, Select OK
        assert quantity.attr("value") == "2"

        FunctionalLibrary.Click(okBtn)
        FunctionalLibrary.Click(overlayOKBtn)

        where: "Getting Test Data from CSV"
        [username,password,site,zone,workcenter,group,serial,serial1] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\117235.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")

    }

    @Unroll("117246 : 'Browser: #envBrowser' - '#envURL'")
    def "07 - 117246: Check multiple wand off for different order is allowed"(){

        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button
        CommonFunctions.login(username, password)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

       // 2, Select Begin scan button
        when:
        at PriorityListPage
        FunctionalLibrary.Click(requestWork)

       // 3, Scan an serial number
        scanSerialNo(serial)

       // 4, Scan an serial number from the different order
        scanSerialNo(serial1)
        FunctionalLibrary.Click(serialScanCancelBtn)

       // 5, Select 35 Wand Off
        FunctionalLibrary.Click(wandOff)

        at WandOff35Page

        then:
       // 6, Verify the wand off screen
        assert quantity.attr("value") == "2"

        FunctionalLibrary.Click(okBtn)
        FunctionalLibrary.Click(overlayOKBtn)

        where: "Getting Test Data from CSV"
        [username,password,site,zone,workcenter,group,serial,serial1] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\117246.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")

    }


}
