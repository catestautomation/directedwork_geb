package Tests.OperatorPortal
import GroovyAutomation.CSVDataSource
import GroovyAutomation.FunctionalLibrary
import Pages.DownTimeOverlay
import Pages.DownnTimeOff55Page
import Pages.OperatorGroupsPage
import Pages.OperatorLoginPage
import Pages.OperatorSitePage
import Pages.OperatorWorkCenterPage
import Pages.OperatorZonePage
import Pages.PriorityListPage
import Pages.RFIDTagUpdateOverlay
import Pages.SetUpTimeOff25Page
import Pages.WandOff35Page
import Utils.CommonFunctions
import geb.driver.CachingDriverFactory
import geb.spock.GebReportingSpec
import org.openqa.selenium.support.ui.WebDriverWait
import spock.lang.Unroll


class DCSTranscationTimeStampErrorHandling extends GebReportingSpec {
    void resetBrowser() {
        def driver = browser.driver
        super.resetBrowser()
        driver.quit()
        CachingDriverFactory.clearCache()
    }

    def setup() {
        CommonFunctions.currentBrowser = getBrowser()
        FunctionalLibrary.currentBrowser = getBrowser()
    }

//    @Unroll("01 - 118152: - Operator Login - Username:'#username' -password:'#password'")
//    def "01 - 118152: Check users are allowed to do successful '35 Wand Off' DCS transaction for MO under 'Wand On Labor (30)'"() {
//
//        // 1, Machine Operator User - Successful Wand On Labor (30) Transaction
//
//        // Login to operator portal
//        CommonFunctions.login(username,password)
//
//        // Operator site, Zone, Workcenter, Group selection
//        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)
//
//        when:
//        at PriorityListPage
//        then:
//        assert heading.text().contains("Workcenter Signed on")
//
//        //Check all tha button status
//        // Enabled buttons Req work, Add row for manual wand on, 50,60,65
//        assert requestWork.attr("class") == "btn btn-primary btnWnd"
//        assert addRowForManualWandOn.attr("class") == "btn btn-primary jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
//        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"
//        assert addToGrouop.attr("class") == "btn  btn-primary btnWnd"
//        assert removeFromGroup.attr("class") == "btn  btn-primary btnWnd"
//
//        // Diabled button status
//        // Disabled butoons 30,35,20,25,55
//        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
//        assert wandOff.attr("class") == "btn btnWnd"
//        assert setUpTImeOn.attr("class") == "btn btnWnd"
//        assert setUpTimeOff.attr("class") == "btn btnWnd"
//        assert downTimeOff.attr("class") == "btn btnWnd"
//
//        // Search MO
//        when:
//        searchMO(order)
//
//        then:
//        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal btn-primary"
//        assert setUpTImeOn.attr("class") == "btn btnWnd btn-primary"
//
//        FunctionalLibrary.Click(wandOn)
//
//        assert rFIDTagContainer.present
//
//        // Update RFID Tag
//        when:
//        CommonFunctions.updateRFIDTag(rfid)
//
//        then:
//        at PriorityListPage
//        assert lowerGridRow.css("background-color") == "rgba(0, 128, 0, 1)"
//
//        //Enabled buttons request work, 35, 50
//        assert requestWork.attr("class") == "btn btn-primary btnWnd"
//        assert wandOff.attr("class") == "btn btnWnd btn-primary"
//        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"
//
//        // Disabled buttons 30,20,25,55
//        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
//        assert setUpTImeOn.attr("class") == "btn btnWnd"
//        assert setUpTimeOff.attr("class") == "btn btnWnd"
//        assert downTimeOff.attr("class") == "btn btnWnd"
//
//        // Check 'Group#' is blinking
//        assert groupBlink.attr("class") == "blink_Group"
//        FunctionalLibrary.Click(wandOff)
//
//        //Wand off (35)
//        at WandOff35Page
//        when:
//        wandOff35(quantity,scrapcode,palletComplete)
//
//        assert informationOverlay.present
//
//        FunctionalLibrary.Click(overlayOKBtn)
//
//        at PriorityListPage
//        then:
//        assert heading.text().contains("Workcenter Signed on")
//        assert groupBlink.attr("class") == ""
//
//        where: "Getting Test Data from CSV"
//        [username, password, site, zone, workcenter, group, order, rfid,quantity,scrapcode,palletComplete] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\118152.csv")
//
//
//    }

    @Unroll("02 - 118164: - Operator Login - Username:'#username' -password:'#password'")
    def "02 - 118164: Check users are allowed to 20 transactions using single / Multiple orders after doing 30 Transaction"() {

        // 1, Machine Operator User - Successful 20 Setup Time On
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button
        at OperatorLoginPage
        when: "Login to Directed work operator portal"
        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 1.9, Search @Order(MO) from upper grid
        // 1.10, Click over the order searched
        at PriorityListPage
        searchMO(order)

        // 1.11, Click on '20 Setup Time On' button
        FunctionalLibrary.Click(setUpTImeOn)

        // 1.12, Button Status are
        // Enabled button status
        then:
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd btn-primary"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"

        // Diabled button status

        assert wandOff.attr("class") == "btn btnWnd"
        assert setUpTImeOn.attr("class") == "btn btnWnd"
        assert downTimeOff.attr("class") == "btn btnWnd"
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"



        assert groupBlink.attr("class") == "blink_Group"

        // 2, Select @Order1 (MO) from upper grid
        when:
        searchMO(order1)

        // 3, Click on 'Wand On Labor (30)' button
        FunctionalLibrary.Click(wandOn)

        // 4, Enter @QTY @SCRAPQTY @SCRAPCDE for all the listed orders and click on OK button

        at SetUpTimeOff25Page
        setUpTimeOff25(qty, scrapQty, scrapcode)

        assert scrapCodeErrorMsg.text().contains("WARNING - REPORTED QTY EXCEEDS PRIOR OPER COMPLETE")
        // 5, click on 'OK' button

        then:
        assert informationOverlay.present
        FunctionalLibrary.Click(overlayOKBtn)

        // 6, Enter @RFID1 for each listed orders
        assert rFIDTagContainer.present

        // Update RFID Tag
        // 7, Click on 'OK' button
        when:
        CommonFunctions.updateRFIDTag(rfid)

        // 8, Check the status of 'Group#'

        then:
        at PriorityListPage
        assert lowerGridRow.css("background-color") == "rgba(0, 128, 0, 1)"
        assert groupBlink.attr("class") == "blink_Group"

        // 9, Check the status of buttons
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert wandOff.attr("class") == "btn btnWnd btn-primary"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"

        // 10, Select 25 button
        FunctionalLibrary.Click(wandOff)

        at WandOff35Page
        when:
        wandOff35(qty, scrapcode, palletComplete)

        // 11, Select Ok
        then:
        FunctionalLibrary.Click(overlayOKBtn)

        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group, order, order1, qty, scrapQty, scrapcode, rfid, palletComplete] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\118164.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")
    }

    @Unroll("03 - 118166: - Operator Login - Username:'#username' -password:'#password'")
    def "03 - 118166: Check users are allowed to 50 transaction after doing same 30 Transaction"() {
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button

        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button
        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        when:
        at PriorityListPage
        then:
        assert heading.text().contains("Workcenter Signed on")

        // 1.9, Click on '50 Indirect/DT On' button
        FunctionalLibrary.Click(downTimeOn)
        // 1.10, Select @DownTimeCode
        // 1.11, Click on button 'OK' button with/without comments
        when:
        CommonFunctions.downTimeReason(code)
        // 1.12, Button Status are
        // Enabled buttons Req work, 55
        then:
        at PriorityListPage
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert addRowForManualWandOn.attr("class") == "btn btn-primary jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert downTimeOff.attr("class") == "btn btnWnd btn-primary"

        // Diabled button status
        // Disabled butoons 30,35,20,25
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert wandOff.attr("class") == "btn btnWnd"
        assert setUpTImeOn.attr("class") == "btn btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd"
        assert lowerGridRow.css("background-color") == "rgba(255, 0, 0, 1)"

        // 1.13, Check 'Group#' is blinking
        assert groupBlink.attr("class") == "blink_Group"

        // 2, Select @Order1 (MO) from upper grid
        when:
        searchMO(order)

        // 3, Click on 'Wand On Labor (30)' button
        FunctionalLibrary.Click(wandOn)

        // 4, Enter @DownTimeCode @Comments and click on OK button

        CommonFunctions.downTimeOff()

        // 5, Enter @RFID1 for each listed orders
        // 6,Click on 'OK' button
        CommonFunctions.updateRFIDTag(rfid)

        // 7, Check the status of 'Group#'
        then:
        at PriorityListPage
        assert groupBlink.attr("class") == "blink_Group"

        // 8,Check the status of buttons
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert wandOff.attr("class") == "btn btnWnd btn-primary"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"

        // 9, Select 35 Wand Off button
        FunctionalLibrary.Click(wandOff)

        //10, Select Pallet complete

        at WandOff35Page
        when:
        wandOff35(qty, scrapcode, palletComplete)

        // 11, Select Ok
        then:

        FunctionalLibrary.Click(overlayOKBtn)

        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group, code, order, rfid, qty, scrapcode, palletComplete] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\118166.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")

    }

    @Unroll("04 - 118177: - Operator Login - Username:'#username' -password:'#password'")
    def "04 - 118177: Check users are allowed to 30 transaction using single / Multiple orders after doing 20 Transaction"() {

        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button

        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button
        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 1.9, Check all tha button status
        // Enabled buttons Req work, Add row for manual wand on, 50,60,65
        at PriorityListPage
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert addRowForManualWandOn.attr("class") == "btn btn-primary jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"
        assert addToGrouop.attr("class") == "btn  btn-primary btnWnd"
        assert removeFromGroup.attr("class") == "btn  btn-primary btnWnd"

        // Diabled button status
        // Disabled butoons 30,35,20,25,55
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert wandOff.attr("class") == "btn btnWnd"
        assert setUpTImeOn.attr("class") == "btn btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd"
        assert downTimeOff.attr("class") == "btn btnWnd"

        // 1.10, Search an @Order(MO) from upper grid
        // 1.11, Click over the Order searched

        when:
        searchMO(order)

        then:
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal btn-primary"
        assert setUpTImeOn.attr("class") == "btn btnWnd btn-primary"

        // 1.12, Click on 'Wand On Labor (30)' button
        when:
        FunctionalLibrary.Click(wandOn)

        // 1.13, Enter @RFID and click on button 'OK'

        CommonFunctions.updateRFIDTag(rfid)
        then:
        at PriorityListPage
        assert lowerGridRow.css("background-color") == "rgba(0, 128, 0, 1)"

        //Enabled buttons request work, 35, 50
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert wandOff.attr("class") == "btn btnWnd btn-primary"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"

        // Disabled buttons 30,20,25,55
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert setUpTImeOn.attr("class") == "btn btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd"
        assert downTimeOff.attr("class") == "btn btnWnd"

        // 1.14, Check 'Group#' is blinking
        assert groupBlink.attr("class") == "blink_Group"

        // 2, Select @Order1 (MO) from upper grid
        when:
        searchMO(order)

        then:
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal btn-primary"
        assert setUpTImeOn.attr("class") == "btn btnWnd btn-primary"

        // 3, Click on '20 Set Up Time On ' button
        FunctionalLibrary.Click(setUpTImeOn)

        // 4, Enter @QTY @SCRAPECODE @PALLETCOMPLETE for all the listed orders and click on OK button
        at WandOff35Page
        when:
        wandOff35(quantity, scrapcode, palletComplete)

        assert informationOverlay.present
        assert scrapCodeErrorMsg.text().contains("WARNING - REPORTED QTY EXCEEDS PRIOR OPER COMPLETE")

        // 5, click on 'OK' button
        FunctionalLibrary.Click(overlayOKBtn)

        // 6, Check the status of 'Group#'
        // 7 ,Check the status of buttons
        then:
        at PriorityListPage
        assert lowerGridRow.css("background-color") == "rgba(255, 255, 0, 1)"
        assert groupBlink.attr("class") == "blink_Group"
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd btn-primary"


        FunctionalLibrary.Click(setUpTimeOff)

        when:
        at SetUpTimeOff25Page
        setUpTimeOff25(quantity, scrapQty, scrapcode)

        then:
        FunctionalLibrary.Click(overlayOKBtn)

        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group, order, rfid, quantity, scrapcode, palletComplete, scrapQty] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\118177.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")

    }

    @Unroll("05 - 118179: - Operator Login - Username:'#username' -password:'#password'")
    def "05 - 118179: Check users are allowed to 50 transaction using single / Multiple orders after doing 20 Transaction"() {

        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button
        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 1.9, Click on '50 Indirect/DT On' button
        at PriorityListPage
        FunctionalLibrary.Click(downTimeOn)

        // 1.10, Select @DownTimeCode
        // 1.11, Click on button 'OK' button with/without comments
        when:
        CommonFunctions.downTimeReason(code)
        // 1.12, Button Status are
        then:
        at PriorityListPage
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert addRowForManualWandOn.attr("class") == "btn btn-primary jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert downTimeOff.attr("class") == "btn btnWnd btn-primary"

        // Diabled button status
        // Disabled butoons 30,35,20,25
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert wandOff.attr("class") == "btn btnWnd"
        assert setUpTImeOn.attr("class") == "btn btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd"

        // 1.13, Check 'Group#' is blinking
        assert groupBlink.attr("class") == "blink_Group"

        // 2, Select @Order1 (MO) from upper grid
        when:
        searchMO(order)

        then:
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal btn-primary"
        assert setUpTImeOn.attr("class") == "btn btnWnd btn-primary"

        // 3, Click on '20 Set Up Time On ' button
        FunctionalLibrary.Click(setUpTImeOn)

        // 4, Enter @DownTimeCode @Comments and click on OK button

        CommonFunctions.downTimeOff()

        // 5, Check the status of 'Group#'
        then:
        at PriorityListPage
        assert groupBlink.attr("class") == "blink_Group"

        // 6, Check the status of buttons
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd btn-primary"

        // 7, Select 25 button

        FunctionalLibrary.Click(setUpTimeOff)

        when:
        at SetUpTimeOff25Page
        setUpTimeOff25(quantity, scrapQty, scrapcode)

        // 8, Select ok
        then:
        FunctionalLibrary.Click(overlayOKBtn)


        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group, code, order, quantity, scrapQty, scrapcode] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\118179.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")
    }

    @Unroll("06 - 118180: - Operator Login - Username:'#username' -password:'#password'")
    def "06 - 118180: Check users are allowed to 20 transaction using single / Multiple orders after doing 50 Transaction "() {

        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button
        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 1.9, Search @Order(MO) from upper grid
        // 1.10, Click over the order searched
        when:
        at PriorityListPage
        searchMO(order)

        // 1.11, Click on '20 Setup Time On' button
        FunctionalLibrary.Click(setUpTImeOn)
        // 1.12, Button Status are
        then:
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd btn-primary"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"

        // Diabled button status

        assert wandOff.attr("class") == "btn btnWnd"
        assert setUpTImeOn.attr("class") == "btn btnWnd"
        assert downTimeOff.attr("class") == "btn btnWnd"
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"

        // 1.13, Check 'Group#' is blinking
        assert groupBlink.attr("class") == "blink_Group"

        // 2, Click on '50 Indirect/DT On' button
        FunctionalLibrary.Click(downTimeOn)

        // 3, Enter @QTY @SCRAPQTY @SCRAPCDE for all the listed orders and click on OK button
        // 4, click on 'OK' button
        when:
        at SetUpTimeOff25Page
        setUpTimeOff25(qty, scrapQty, scrapcode)

        then:
        assert informationOverlay.present

        FunctionalLibrary.Click(overlayOKBtn)

        // 5, Select @DownTime Code
        // 6, Click on button 'OK' button with/without comments

        CommonFunctions.downTimeReason(code)

        // 7, Check the status of 'Group#'
        then:
        at PriorityListPage
        assert groupBlink.attr("class") == "blink_Group"
        // 8, Check the status of buttons
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"
        assert downTimeOff.attr("class") == "btn btnWnd btn-primary"

        // 9, Select 55 Indirect DT off
        FunctionalLibrary.Click(downTimeOff)

        // 10, Select ok
        CommonFunctions.downTimeOff()

        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group, order, qty, scrapQty, scrapcode, code] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\118180.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")
    }

    @Unroll("07 - 118181:- Operator Login - Username:'#username' -password:'#password'")
    def "07 - 118181: Check users are allowed to 30 transaction using single / Multiple orders after doing 50 Transaction"() {
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button
        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        when:
        at PriorityListPage

        // 1.9, Check all tha button mode
        // Enabled buttons Req work, Add row for manual wand on, 50,60,65
        then:
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert addRowForManualWandOn.attr("class") == "btn btn-primary jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"
        assert addToGrouop.attr("class") == "btn  btn-primary btnWnd"
        assert removeFromGroup.attr("class") == "btn  btn-primary btnWnd"

        // Diabled button status
        // Disabled butoons 30,35,20,25,55
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert wandOff.attr("class") == "btn btnWnd"
        assert setUpTImeOn.attr("class") == "btn btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd"
        assert downTimeOff.attr("class") == "btn btnWnd"

        // 1.10, Search @Order(MO) from upper grid
        // 1.11, Click over the order searched
        when:
        searchMO(order)

        // 1.12, Click on 'Wand On Labor (30)' button
        FunctionalLibrary.Click(wandOn)

        // 1.13, Enter @RFID and click on button 'OK'
        CommonFunctions.updateRFIDTag(rfid)

        then:
        at PriorityListPage
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"
        assert wandOff.attr("class") == "btn btnWnd btn-primary"

        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert setUpTImeOn.attr("class") == "btn btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd"
        assert downTimeOff.attr("class") == "btn btnWnd"

        // 1.14, Check 'Group#' is blinking
        assert groupBlink.attr("class") == "blink_Group"

        // 2, Click on '50 Indirect/DT On' button
        FunctionalLibrary.Click(downTimeOn)

        // 3, Enter @QTY @SCRAPECODE @PALLETCOMPLETE for all the listed orders and click on OK button
        at WandOff35Page
        when:
        wandOff35(qty, scrapcode, palletcomplete)

        // 4, click on 'OK' button
        FunctionalLibrary.Click(overlayOKBtn)

        // 5, Select @DownTime Code
        // 6, Click on button 'OK' button with/without comments
        CommonFunctions.downTimeReason(code)

        // 7, Check the status of 'Group#9
        at PriorityListPage
        then:
        assert groupBlink.attr("class") == "blink_Group"

        // 8, Check the status of buttons
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"
        assert downTimeOff.attr("class") == "btn btnWnd btn-primary"

        // 9, Select 55 Indirect DT Off
        FunctionalLibrary.Click(downTimeOff)

        // 10, Select ok
        CommonFunctions.downTimeOff()

        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group, order, rfid, qty, scrapcode, palletcomplete, code] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\118181.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")
    }

    @Unroll("08 - 118548: - Operator Login - Username:'#username' -password:'#password'")
    def "08 - 118548: Validate 'RFID' fields using valid and invalid values"() {

        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button
        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 2, Select @Order (MO) from upper grid
        at PriorityListPage
        when:
        searchMO(order)

        // 3, Click on 'Wand On Labor (30)' button
        FunctionalLibrary.Click(wandOn)

        // 4, Enter @InvalidRfid
        at RFIDTagUpdateOverlay
        FunctionalLibrary.SetText(tagValueField, invalidRFID)

        // 5, Click ok button
        FunctionalLibrary.Click(updateTagBtn)
        then:
        def alertText = driver.switchTo().alert().getText()

        assert alertText == "Please enter valid RFID"

        // 6, Click ok button
        driver.switchTo().alert().accept()

        // 7, Enter @ValidRFID
        // 8, Click ok button
        CommonFunctions.updateRFIDTag(rfid)

        // 9, Select 35 button
        at PriorityListPage
        FunctionalLibrary.Click(wandOff)

        // 10, Select Pallet Complete

        when:
        at WandOff35Page
        wandOff35(qty, scrapcode, palletcomplete)

        // 11, Select Ok
        then:
        FunctionalLibrary.Click(overlayOKBtn)

        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group, order, invalidRFID, rfid, qty, scrapcode, palletcomplete, code] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\118548.csv")

        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")
    }

    @Unroll("09 - 115254: - Operator Login - Username:'#username' -password:'#password'")
    def "09 - 115254: Valide the fields in the 50 and 55 screens"() {

        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button
        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        //Pre -Requisite


        at PriorityListPage

        assert wandOn.present
        assert wandOff.present
        assert setUpTImeOn.present
        assert setUpTimeOff.present
        assert downTimeOn.present
        assert downTimeOff.present
        assert requestWork.present

        when:
        searchMO(order)

        FunctionalLibrary.Click(wandOn)

        // Update RFID Tag

        CommonFunctions.updateRFIDTag(rfid)

        then:
        at PriorityListPage
        assert lowerGridRow.css("background-color") == "rgba(0, 128, 0, 1)"

        FunctionalLibrary.Click(downTimeOn)

        at WandOff35Page
        when:
        wandOff35(qty, scrapQty, palletcomplete)

        FunctionalLibrary.Click(overlayOKBtn)

        at DownTimeOverlay
        FunctionalLibrary.Click(downTimeCodeOKBtn)

        def alertText = driver.switchTo().alert().getText()

        then:
        assert alertText == "Please select valid downtime code"

        driver.switchTo().alert().accept()

        CommonFunctions.downTimeReason(code)

        at PriorityListPage
        FunctionalLibrary.Click(downTimeOff)

        CommonFunctions.downTimeOff()

        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group, order, rfid, qty, scrapQty, palletcomplete, code] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\115254.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")
    }

    @Unroll("10 - 114977: - Operator Login - Username:'#username' -password:'#password'")
    def "10 - 114977:- Validate the fields while doing 35 for an order which 30 is already done"() {
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 2, Observe the Priority List Screen
        at PriorityListPage
        assert wandOn.present
        assert wandOff.present
        assert setUpTImeOn.present
        assert setUpTimeOff.present
        assert downTimeOn.present
        assert downTimeOff.present
        assert requestWork.present

        //Pre -Requisite
        when:
        searchMO(order)

        FunctionalLibrary.Click(wandOn)

        // Update RFID Tag

        CommonFunctions.updateRFIDTag(rfid)

        // 3, Observe the upper grid
        then:
        at PriorityListPage
        assert lowerGridRow.css("background-color") == "rgba(0, 128, 0, 1)"

        // 4, Select any order from the upper grid
        when:
        searchMO(order)

        // 5, Select Wand On Labor 30 button
        FunctionalLibrary.Click(wandOn)

        // 6, Verify the 35 screen
        at WandOff35Page

        // 7, Select Ok without filling any field
        FunctionalLibrary.Click(okBtn)

        def alertText = driver.switchTo().alert().getText()

        then:
        assert alertText == "Please select pallet complete value"

        driver.switchTo().alert().accept()

        // 8, Select Qty and Enter Scrap code as Rw
        when:
        wandOff35(qty, scrapcode, palletcomplete)

        // 9, Select Ok in the Hard error
        then:
        FunctionalLibrary.Click(overlayOKBtn)

        // 10, Enter Valid Scrap code or leave blank
        // 11, Select pallet complete from drop down and click on "OK" button
        when:
        wandOff35(qty, scrapcode1, palletcomplete)
        then:
        FunctionalLibrary.Click(overlayOKBtn)

        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group, order, rfid, qty, scrapcode, palletcomplete, scrapcode1] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\114977.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")

    }

    @Unroll("11 - 114932: - Operator Login - Username:'#username' -password:'#password'")
    def "11 - :- Verify the alert message when user enters Qty within range in the wand off screen"() {

        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 2, Observe the Priority List Screen
        at PriorityListPage
        assert wandOn.present
        assert wandOff.present
        assert setUpTImeOn.present
        assert setUpTimeOff.present
        assert downTimeOn.present
        assert downTimeOff.present
        assert requestWork.present

        when:
        searchMO(order)

        FunctionalLibrary.Click(wandOn)
        // Update RFID Tag
        CommonFunctions.updateRFIDTag(rfid)
        // 3, Observe the upper grid
        then:
        at PriorityListPage
        assert lowerGridRow.css("background-color") == "rgba(0, 128, 0, 1)"

        // 4, Select any order from the upper grid
        when:
        searchMO(order)

        // 5,Select Wand On Labor 30 button
        FunctionalLibrary.Click(wandOn)

        // 6, Verify the 35 screen
        at WandOff35Page

        // 7, Enter Qty which exceeds the Qty required but within the allow percentage range
        // 8, Select pallet complete and click ok
        wandOff35(qty,scrapcode,palletcomplete)

        // 9, Verify the soft error alert
        assert informationOverlay.present

        // 10, Select Cancel buton
        FunctionalLibrary.Click(overlayCancelBtn)

        // 11, Select Ok in the Alert pop up
        FunctionalLibrary.Click(okBtn)
        then:
        FunctionalLibrary.Click(overlayOKBtn)

        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group, order, rfid,qty, scrapcode, palletcomplete] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\114932.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")
    }

    @Unroll("12 - 114934: - Operator Login - Username:'#username' -password:'#password'")
    def "12 - 114934:- Verify that soft error throws when a MO is completed by someoneelse"(){
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)



        when:

        // 2, Observe the Priority List Screen
        at PriorityListPage
        assert wandOn.present
        assert wandOff.present
        assert setUpTImeOn.present
        assert setUpTimeOff.present
        assert downTimeOn.present
        assert downTimeOff.present
        assert requestWork.present

        // 3, Select an order from Upper Grid
        searchMO(order)

        // 4, Select Wand On 30
        FunctionalLibrary.Click(wandOn)

        // 5, Update RFID Tag
        CommonFunctions.updateRFIDTag(rfid)

        go "http://stagemfg.ashleyfurniture.com/Ashley.Manufacturing.Directedwork.Operators/Ashley/login.html"

        // 6, Login to Operator with another credentials and select same configuration
        CommonFunctions.login(username1, password1)

        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 7, select the same order which is already selected by another user

        at PriorityListPage
        searchMO(order)

        // 8, Select Wand On 30
        FunctionalLibrary.Click(wandOn)

        // 9,Select pallet complete and click ok
        at WandOff35Page

        wandOff35(qty,scrapcode,palletcomplete)

        FunctionalLibrary.Click(overlayOKBtn)

        CommonFunctions.updateRFIDTag(rfid)


        go "http://stagemfg.ashleyfurniture.com/Ashley.Manufacturing.Directedwork.Operators/Ashley/login.html"

        // 10, Login as operator with previous login credentials
        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 11, Select Wand Off 35 button
        at PriorityListPage
        FunctionalLibrary.Click(wandOff)

        // 12, Select pallet complete no and click ok button

        at WandOff35Page
        wandOff35(qty,scrapcode,palletcomplete1)
        FunctionalLibrary.Click(overlayOKBtn)
        then:
        at PriorityListPage

        assert wandOn.present


        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group, order, rfid,username1,password1,qty, scrapcode, palletcomplete,palletcomplete1] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\114934.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")

    }

    @Unroll("13 - 114953:- Operator Login - Username:'#username' -password:'#password'")
    def "13 - 114953:- Verify that soft error throws when Completed Pieces/ Scrap Pieces/Rework Qty is more than the allowed percentage"(){

        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 2, Observe the Priority List Screen
        at PriorityListPage
        assert wandOn.present
        assert wandOff.present
        assert setUpTImeOn.present
        assert setUpTimeOff.present
        assert downTimeOn.present
        assert downTimeOff.present
        assert requestWork.present

        // 3, Select an order from Upper Grid
        when:
        searchMO(order)

        // 4 , Select Wand On 30
        FunctionalLibrary.Click(wandOn)
        // Update RFID Tag
        CommonFunctions.updateRFIDTag(rfid)

        // 5, Select Wand Off 35

        at PriorityListPage
        FunctionalLibrary.Click(wandOff)

        // 6, Enter Qty which is beyond the required level
        at WandOff35Page
        wandOff35(qty,scrapcode,palletcomplete)
        then:
        assert informationOverlay.present

        // 7, Select ok
        FunctionalLibrary.Click(overlayOKBtn)

        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group, order, rfid,qty, scrapcode, palletcomplete] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\114953.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")
    }

    @Unroll("14 - 114955:- Operator Login - Username:'#username' -password:'#password'")
    def "14 - 114955:- Verify the soft error when scrap code is invalid"(){
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 2, Observe the Priority List Screen
        at PriorityListPage
        assert wandOn.present
        assert wandOff.present
        assert setUpTImeOn.present
        assert setUpTimeOff.present
        assert downTimeOn.present
        assert downTimeOff.present
        assert requestWork.present

        // 3, Select an order from Upper Grid
        when:
        searchMO(order)

        // 4 , Select Wand On 30
        FunctionalLibrary.Click(wandOn)
        // Update RFID Tag
        CommonFunctions.updateRFIDTag(rfid)

        // 5, Select Wand Off 35
        at PriorityListPage
        FunctionalLibrary.Click(wandOff)
        at WandOff35Page


        // 6, Enter Qty as minimal Qty
        // 7, Select the pallet complete value from dropdown
        // 8, Select invalid scrap Code and Ok buton
        wandOff35(qty,scrapcode,palletcomplete)
        then:
        assert informationOverlay.present
        assert scrapCodeErrorMsg.text().contains("INVALID SCRAP CODE")


        // 9, Select Ok in the alert pop up
        FunctionalLibrary.Click(overlayOKBtn)

        when:
        wandOff35(qty,scrapcode1,palletcomplete)

        then:
        assert informationOverlay.present
        FunctionalLibrary.Click(overlayOKBtn)

        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group, order, rfid,qty, scrapcode, palletcomplete,scrapcode1] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\114955.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")
    }

    @Unroll("15 - 118562:- Operator Login - Username:'#username' -password:'#password'")
    def "15 - 118562:- Verify the operator 'Logout' functionality when DCS transaction is ON "(){
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 1.9, Check all the button mode// 1.9, Check all the button mode
        at PriorityListPage
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert addRowForManualWandOn.attr("class") == "btn btn-primary jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"
        assert addToGrouop.attr("class") == "btn  btn-primary btnWnd"
        assert removeFromGroup.attr("class") == "btn  btn-primary btnWnd"

        // Diabled button status
        // Disabled butoons 30,35,20,25,55
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert wandOff.attr("class") == "btn btnWnd"
        assert setUpTImeOn.attr("class") == "btn btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd"
        assert downTimeOff.attr("class") == "btn btnWnd"

        // 1.10, Search an @Order(MO) from upper grid
        // 1.11, Click over the Order searched
        when:
        searchMO(order)

        // 1.12, Click on 'Wand On Labor (30)' button
        FunctionalLibrary.Click(wandOn)

        // 1.13, Enter @RFID and click on button 'OK'
        CommonFunctions.updateRFIDTag(rfid)

        // 1.14, Check 'Group#' is blinking
        at PriorityListPage
        assert groupBlink.attr("class") == "blink_Group"

        // 2, Select Logout button
        CommonFunctions.logOut()

        // 3, Select OK in the alert pop up
        def alertText = driver.switchTo().alert().getText()

        assert alertText == "Please Wand Off (35) the selected order"

        driver.switchTo().alert().accept()

        // 4, Select 35 Wand Off button
        at PriorityListPage
        FunctionalLibrary.Click(wandOff)

        // 5, Select Pallet Complete 'Yes' or 'No'
        at WandOff35Page
        wandOff35(qty,scrapcode,palletcomplete)

        then:
        // 6, Select Ok
        assert informationOverlay.present
        FunctionalLibrary.Click(overlayOKBtn)
        CommonFunctions.logOut()

        // 7, Machine Operator User - Successful 20 Setup Time On
        // 7.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 7.2, Enter valid @Operatorusername in Username field
        // 7.3, Enter valid @Operatorpassword in Password field
        // 7.4, Click on "Login" button

        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 7.5, Select @OperatorSite
        // 7.6, Select @OperatorZone
        // 7.7, Select @OperatorWorkCenter
        // 7.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 7.9, Search @Order(MO) from upper grid
        // 7.10, Click over the order searched
        when:
        at PriorityListPage
        searchMO(order)

        // 7.11, Click on '20 Setup Time On' button
        FunctionalLibrary.Click(setUpTImeOn)

        // 7.12, Button Status are
        then:
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd btn-primary"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"

        // Diabled button status

        assert wandOff.attr("class") == "btn btnWnd"
        assert setUpTImeOn.attr("class") == "btn btnWnd"
        assert downTimeOff.attr("class") == "btn btnWnd"
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"

        // 7.13, Check 'Group#' is blinking
        assert groupBlink.attr("class") == "blink_Group"

        // 8, Select Logout button
        CommonFunctions.logOut()

        def alertText1 = driver.switchTo().alert().getText()

        assert alertText1 == "Please Setup Off (25) the selected order"

        // 9, Select OK in the alert pop up
        driver.switchTo().alert().accept()

        // 10, Select 25 Set Up Off button
        at PriorityListPage
        FunctionalLibrary.Click(setUpTimeOff)

        // 11, Select Ok
        when:
        at SetUpTimeOff25Page
        setUpTimeOff25(qty, scrapQty, scrapcode)

        then:
        FunctionalLibrary.Click(overlayOKBtn)

        CommonFunctions.logOut()

        // 12, Machine Operator User - Successful 50 Indirect/DT On
        // 12.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 12.2, Enter valid @Operatorusername in Username field
        // 12.3, Enter valid @Operatorpassword in Password field
        // 12.4, Click on "Login" button

        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 12.5, Select @OperatorSite
        // 12.6, Select @OperatorZone
        // 12.7, Select @OperatorWorkCenter
        // 12.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 12.9, Click on '50 Indirect/DT On' button
        at PriorityListPage
        FunctionalLibrary.Click(downTimeOn)

        // 12.10, Select @DownTimeCode
        // 12.11, Click on button 'OK' button with/without comments
        when:
        CommonFunctions.downTimeReason(code)
        // 1.12, Button Status are
        then:
        at PriorityListPage
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert downTimeOff.attr("class") == "btn btnWnd btn-primary"

        // Diabled button status
        // Disabled butoons 30,35,20,25
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert wandOff.attr("class") == "btn btnWnd"
        assert setUpTImeOn.attr("class") == "btn btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd"

        // 12.13, Check 'Group#' is blinking
        assert groupBlink.attr("class") == "blink_Group"

        // 13, Select Logout button
        CommonFunctions.logOut()

        // 14, Select OK in the alert pop up
        def alertText2 = driver.switchTo().alert().getText()

        assert alertText2 == "Please DownTime Off (55) the selected order"

        driver.switchTo().alert().accept()

        // 15, Select 55 Indirect DT Off button
        at PriorityListPage
        FunctionalLibrary.Click(downTimeOff)

        // 16, Select Ok
        CommonFunctions.downTimeOff()

        // 17, Select Logout Button
        CommonFunctions.logOut()

        then:
        at OperatorLoginPage
        assert loginScreen.present


        where: "Getting Test Data from CSV"
        [username,password,site,zone,workcenter,group,order,rfid,qty,scrapcode,palletcomplete,scrapQty,code] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\118562.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")

    }

    @Unroll("16 - 118564:- Operator Login - Username:'#username' -password:'#password'")
    def "16 - Verify the orders in lower grid are retained after re-opening the closed browser "(){
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        when:
        at PriorityListPage
        searchMO(order)

        // 2,Select an MO from the Upper Grid
        // 3,Select 30 wand On button
        FunctionalLibrary.Click(wandOn)

        // 4, Enter @RFID and click on button 'OK'
        CommonFunctions.updateRFIDTag(rfid)

        // 5, Close the Browser
        // 6, Launch the browser and Navigate to the same page
        go "http://stagemfg.ashleyfurniture.com/Ashley.Manufacturing.Directedwork.Operators/Ashley/login.html"
        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 7, Verify the lower gird

        at PriorityListPage
        assert lowerGridRow.css("background-color") == "rgba(0, 128, 0, 1)"
        // 8, Select 35 Wand off button
        FunctionalLibrary.Click(wandOff)

        // 9, Select Pallet Complete
        at WandOff35Page

        wandOff35(qty,scrapcode,palletcomplete)

        // 10, Select ok
        then:
        FunctionalLibrary.Click(overlayOKBtn)

        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group, order, rfid,qty, scrapcode, palletcomplete] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\118564.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")



    }

    @Unroll("17 - 124825:- Operator Login - Username:'#username' -password:'#password'")
    def "17 - Perform 60 and 65 transaction when he is out of group terminal"(){
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)
        FunctionalLibrary.WaitForPageLoad(50)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 1.9, Check all the button mode
        at PriorityListPage
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert addRowForManualWandOn.attr("class") == "btn btn-primary jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"
        assert addToGrouop.attr("class") == "btn  btn-primary btnWnd"
        assert removeFromGroup.attr("class") == "btn  btn-primary btnWnd"

        // Diabled button status
        // Disabled butoons 30,35,20,25,55
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert wandOff.attr("class") == "btn btnWnd"
        assert setUpTImeOn.attr("class") == "btn btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd"
        assert downTimeOff.attr("class") == "btn btnWnd"

        // 1.10, Select '60 Add To Group' button
        FunctionalLibrary.Click(addToGrouop)

        // 1.11, Enter @Employee number who is removed from other group
        // 1.12, Select Ok
        // 1.13, Select ok in the pop up
        when:
        addEmployeeToGroup(empno)

        // 2, Check the status of 'Group#'
        then:
        assert groupBlink.attr("class") == ""

        // 3, Check the status of buttons
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert addRowForManualWandOn.attr("class") == "btn btn-primary jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"
        assert addToGrouop.attr("class") == "btn  btn-primary btnWnd"
        assert removeFromGroup.attr("class") == "btn  btn-primary btnWnd"

        // Diabled button status
        // Disabled butoons 30,35,20,25,55
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert wandOff.attr("class") == "btn btnWnd"
        assert setUpTImeOn.attr("class") == "btn btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd"
        assert downTimeOff.attr("class") == "btn btnWnd"

        // 4, Select 65 button
        FunctionalLibrary.Click(removeFromGroup)

        // 5, Verify the Pop up
        // 6, Enter @Employee number
        // 7, select ok
        when:
        removeEmpFromGroup(empno)

        // 8, Verify the Confirmation pop up
        assert infoAlertForRemoveEmpFromGroup == "The Employee "+empno+" has been removed from "+group+"."

        // 9, Select ok in the Confirmation pop up
        FunctionalLibrary.Click(infoAlert)

        // 10, Select ok in the pop up

        FunctionalLibrary.Click(cancelBtn)

        // 11, Check the status of 'Group#'
        then:
        assert groupBlink.attr("class") == ""

        // 12, Check the status of buttons
        assert requestWork.attr("class") == "btn btn-primary btnWnd"
        assert addRowForManualWandOn.attr("class") == "btn btn-primary jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"
        assert addToGrouop.attr("class") == "btn  btn-primary btnWnd"
        assert removeFromGroup.attr("class") == "btn  btn-primary btnWnd"

        // Diabled button status
        // Disabled butoons 30,35,20,25,55
        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
        assert wandOff.attr("class") == "btn btnWnd"
        assert setUpTImeOn.attr("class") == "btn btnWnd"
        assert setUpTimeOff.attr("class") == "btn btnWnd"
        assert downTimeOff.attr("class") == "btn btnWnd"

        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group,empno] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\124825.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")
    }


//    @Unroll("18 - 124835:- Operator Login - Username:'#username' -password:'#password'")
//    def "Perform 65 and then 60 from group terminal when Group  not active"(){
//        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
//        // 1.2, Enter valid @Operatorusername in Username field
//        // 1.3, Enter valid @Operatorpassword in Password field
//        // 1.4, Click on "Login" button
//
//        CommonFunctions.login(username, password) FunctionalLibrary.WaitForPageLoad(50) 
//
//        // 1.5, Select @OperatorSite
//        // 1.6, Select @OperatorZone
//        // 1.7, Select @OperatorWorkCenter
//        // 1.8, Select @Group and click on 'OK' button
//        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)
//
//        // 2, Check all the button mode
//        at PriorityListPage
//        assert requestWork.attr("class") == "btn btn-primary btnWnd"
//        assert addRowForManualWandOn.attr("class") == "btn btn-primary jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
//        assert downTimeOn.attr("class") == "btn  btn-primary btnWnd"
//        assert addToGrouop.attr("class") == "btn  btn-primary btnWnd"
//        assert removeFromGroup.attr("class") == "btn  btn-primary btnWnd"
//
//        // Diabled button status
//        // Disabled butoons 30,35,20,25,55
//        assert wandOn.attr("class") == "btn jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
//        assert wandOff.attr("class") == "btn btnWnd"
//        assert setUpTImeOn.attr("class") == "btn btnWnd"
//        assert setUpTimeOff.attr("class") == "btn btnWnd"
//        assert downTimeOff.attr("class") == "btn btnWnd"
//
//        // 3, Select 65 button
//        FunctionalLibrary.Click(removeFromGroup)
//
//        // 4, Verify the Pop up
//        assert removeEmpNumber.present
//
//        // 5, Enter @Employee number
//        // 6, select ok
//        when:
//        removeEmpFromGroup(empno)
//        FunctionalLibrary.Click(cancelBtn)
//
//        // 7, Select '60 Add To Group' button
//        FunctionalLibrary.Click(addToGrouop)
//
//        // 8, Enter @Employee number who is removed from other group
//        // 9, Select Ok
//        addEmployeeToGroup(empno)
//
//
//
//
//
//
//
//
//
//
//    }



}

