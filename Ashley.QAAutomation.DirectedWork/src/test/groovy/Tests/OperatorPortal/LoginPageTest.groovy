package Tests.OperatorPortal

import GroovyAutomation.CSVDataSource
import Pages.OperatorGroupsPage
import Pages.OperatorLoginPage
import Pages.OperatorSitePage
import Pages.OperatorWorkCenterPage
import Pages.OperatorZonePage
import Pages.PriorityListPage
import Utils.CommonFunctions
import geb.spock.GebReportingSpec
import geb.driver.CachingDriverFactory
import GroovyAutomation.FunctionalLibrary
import spock.lang.Unroll

class LoginPageTest extends GebReportingSpec{

    void resetBrowser() {
        def driver = browser.driver
        super.resetBrowser()
        driver.quit()
        CachingDriverFactory.clearCache()
    }

    def setup() {
        CommonFunctions.currentBrowser = getBrowser()
        FunctionalLibrary.currentBrowser = getBrowser()
    }

    @Unroll("01 - 132398: - Operator Login - Username:'#username' -password:'#password' - Browser:'#envBrowser' - '#envURL'\"")
    def "01 - 132398: Validate the Operators Login functionality"() {

        // 1, Launch "Operators" @URL and click on "Enter" button
        // 2,Check for the '@Username' & '@Password' fields
        // 3,Check for the "Login" button

        CommonFunctions.login(username,password)

        when:
        at OperatorSitePage
        then:
        assert heading.text() == "LIST OF SITES"

        where: "Getting Test Data from CSV"
        [username, password] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\132398.csv")

        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")
    }


    @Unroll("02 - 120916: - Operator Login - UserName:'#username' -password:'#password'  - Browser:'#envBrowser' - '#envURL'")
    def "02 - 120916: Check the browser tab based on selection (Site, Zone and Workcenter)"() {

        // 1, Launch "Operators" @URL and click on "Enter" button
        // 2,Check for the '@Username' & '@Password' fields
        // 3,Check for the "Login" button
        // 4,Select Login Button

        CommonFunctions.login(username,password)

        // 5,Select the @Site
        when:
        at OperatorSitePage
        operatorSiteSelection(site)

        then:
        assert heading.text().contains("LIST OF ZONES FOR THE SITE")

        // 6,Select Browser Back
        when:
        driver.navigate().back()
        FunctionalLibrary.WaitForPageLoad(50)
        then:
        assert heading.text().contains("LIST OF ZONES FOR THE SITE")

        // 7,Select @Zone
        when:
        at OperatorZonePage
        operatorZoneSelection(zone)
        then:
        assert  heading.text().contains("LIST OF WORKCENTER FOR THE ZONE")

        // 8,Select Browser Back
        when:
        driver.navigate().back()
        FunctionalLibrary.WaitForPageLoad(50)
        then:
        assert  heading.text().contains("LIST OF WORKCENTER FOR THE ZONE")

        // 9,Select Workcentre
        when:
        at OperatorWorkCenterPage
        operatorWorkCenterSelection(workcenter)

        then:
        assert groupContainer.present

        // 10, Select Browser Back
        driver.navigate().back()
        FunctionalLibrary.WaitForPageLoad(50)
        assert  groupContainer.present

        // 11, Select Group
        when:
        at OperatorGroupsPage
        operatorGroupSelection(group)
        // 12, Select Browser Back

        then:
        driver.navigate().back()
        FunctionalLibrary.WaitForPageLoad(50)

        then:
        at PriorityListPage
        assert heading.text().contains("Workcenter Signed on")

        where: "Getting Test Data from CSV"
        [username, password,site,zone,workcenter,group] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\132398.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")
    }



}





