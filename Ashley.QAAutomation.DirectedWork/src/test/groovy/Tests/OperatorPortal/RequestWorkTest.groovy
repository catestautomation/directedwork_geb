package Tests.OperatorPortal
import GroovyAutomation.CSVDataSource
import GroovyAutomation.FunctionalLibrary
import Pages.PartsCarrierTaskListPage
import Pages.PriorityListPage
import Pages.RequestWorkPage
import Utils.CommonFunctions
import geb.driver.CachingDriverFactory

import geb.spock.GebReportingSpec
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.StaleElementReferenceException
import spock.lang.Unroll


class RequestWorkTest extends GebReportingSpec {
    void resetBrowser() {
        def driver = browser.driver
        super.resetBrowser()
        driver.quit()
        CachingDriverFactory.clearCache()
    }

    def setup() {
        CommonFunctions.currentBrowser = getBrowser()
        FunctionalLibrary.currentBrowser = getBrowser()

    }

    @Unroll("118570 : 'Browser: #envBrowser' - '#envURL'")
    def "01 - 118570: Verify User operator is able to request Tasks"(){


        // 1, Machine Operator User - Successful 20 Setup Time On
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

         // 2, Click on the Request work button

        at PriorityListPage
        FunctionalLibrary.Click(requestWork)

        // 3, Click @Task button

        when:
        at RequestWorkPage

//         status = getBrowser().js.exec("window.onbeforeunload = function(){ return 'Reload?';}")
//        status = getBrowser().js.exec("function LoaderStopper(){var highestTimeoutId=setTimeout(';');for(var i=0;i<highestTimeoutId;i++){clearTimeout(i);}}")
//
//        JavascriptExecutor jse = (JavascriptExecutor)driver;
//        status = jse.executeScript("function LoaderStopper(){var highestTimeoutId=setTimeout(';');for(var i=0;i<highestTimeoutId;i++){clearTimeout(i);}}")



        def Rows = null


        List<String> tableValues = new ArrayList<String>()
        int flag = 0
        def taskList =$("#taskListStatus").find("table").first()
        def priority = null
        while(flag == 0) {
            FunctionalLibrary.WaitForPageLoad()
            taskList =$("#taskListStatus").find("table").first()
            Rows = taskList.find("tr.even-row").findAll()

            for (int i = 0; i < Rows.size(); i++) {

                try {
                    FunctionalLibrary.WaitForPageLoad()
                    taskList =$("#taskListStatus").find("table").first()
                    Rows = taskList.find("tr.even-row").findAll()
                    priority = Rows.getAt(i).find("td").first().text()
                    if(!tableValues.contains(priority)){
                        tableValues.add(priority)
                    }

                    if (Rows.size() == i+1) {
                        flag = 1
                        break;
                    }
                }

                catch (StaleElementReferenceException ex){}

                FunctionalLibrary.WaitForPageLoad()
                taskList =$("#taskListStatus").find("table").first()
                Rows = taskList.find("tr.even-row").findAll()

             }
        }
        for(int i =0; i< tableValues.size() ; i++) {
            def firstValue = tableValues.getAt(0)
            if (firstValue >= tableValues.getAt(i)) {}
                else{
                throw new Exception("Priority value is not in descending order")
            }
        }


        def Zone = zone.toUpperCase()
        def WorkCenter = workcenter.toUpperCase()
        assert heading.text().toUpperCase() == "ZONE - " + Zone + " / WORKCENTER - " + WorkCenter + ""

        try {
            workRequestSelection(workrequest)
        }
        catch(Exception ex){
            workRequestSelection(workrequest)
        }
        // 4, Check the color of assigned task

        then:
        assert taskListStatus.css("background").contains("to(rgb(255, 102, 102)))")

        // 5, Check the Priority, Workcenter, Task and Request Time of assigned task



        // 6, Parts Carrier User - Steps to Login DirectedWork Site
        // 6.1, Launch "Parts Carrier" @URL-PC and click on "Enter" button
        // 6.2, Enter Valid @username-PC in Username field
        // 6.3, Enter Valid @password-PC in Password field
        // 6.4, Click on "Login" button
        when:
        go "http://stagemfg.ashleyfurniture.com/Ashley.Manufacturing.Directedwork.partscarriers/Ashley/login.html"
        CommonFunctions.login(username, password)

        // 6.5, Select @Site-PC
        // 6.6, Select @Zone-PC
        // 6.7, Select Yes in the PIV check page
        CommonFunctions.partsCarrierSiteZonePIVCheck(site,zone,pivcheck)

        // 8, Check the parts carrier Task list page for assigned @Task
        then:
        at PartsCarrierTaskListPage
        assert heading.present


        // 9, Check the color of assigned task

        // 10, Check the Priority, Workcenter, Task and Request Time of assigned task

        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group,workrequest,pivcheck] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\118570.csv")

        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")

    }

    @Unroll("118571 : 'Browser: #envBrowser' - '#envURL'")
    def "02 - 118571: Verify user Operator able to request Parts Manually using option 'ASSBLY'"(){
        // 1, Machine Operator User - Successful 20 Setup Time On
        // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
        // 1.2, Enter valid @Operatorusername in Username field
        // 1.3, Enter valid @Operatorpassword in Password field
        // 1.4, Click on "Login" button

        CommonFunctions.login(username, password)

        // 1.5, Select @OperatorSite
        // 1.6, Select @OperatorZone
        // 1.7, Select @OperatorWorkCenter
        // 1.8, Select @Group and click on 'OK' button
        CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group)

        // 2, Click on the Request work button

        at PriorityListPage
        FunctionalLibrary.Click(requestWork)

        at RequestWorkPage
        def Zone = zone.toUpperCase()
        def WorkCenter = workcenter.toUpperCase()
        assert heading.text().toUpperCase()== "ZONE - "+Zone+" / WORKCENTER - "+WorkCenter+""

        // 3, Click on 'ASSBLY' button
        when:
        workRequestSelection("ASSBLY")

        // 4, Verify the ASSBLY Task popup
        // 5, Click on 'Cancel' button
        then:
        FunctionalLibrary.Click(btnCancel)

        // 6, Click on 'ASSBLY' button
        when:
        workRequestSelection("ASSBLY")

        then:
        //7, Enter @PartNumber and click ok button
        FunctionalLibrary.SetText(assemblyTaskValue,"A123E")
        FunctionalLibrary.Click(btnOk)

        // 8, check the color of assigned ASSBLY task


        where: "Getting Test Data from CSV"
        [username, password, site, zone, workcenter, group,workrequest,pivcheck] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\124825.csv")

    }



}
