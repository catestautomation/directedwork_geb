package Tests.PartsCarrierPortal


import GroovyAutomation.CSVDataSource
import Pages.OperatorGroupsPage
import Pages.OperatorLoginPage
import Pages.OperatorSitePage
import Pages.OperatorWorkCenterPage
import Pages.OperatorZonePage
import Pages.PartsCarrierSitePage
import Pages.PriorityListPage
import Utils.CommonFunctions
import geb.spock.GebReportingSpec
import geb.driver.CachingDriverFactory
import GroovyAutomation.FunctionalLibrary
import spock.lang.Unroll

class PartsCarrierLoginTest extends GebReportingSpec {
    void resetBrowser() {
        def driver = browser.driver
        super.resetBrowser()
        driver.quit()
        CachingDriverFactory.clearCache()
    }

    def setup() {
        CommonFunctions.currentBrowser = getBrowser()
        FunctionalLibrary.currentBrowser = getBrowser()
    }

    @Unroll("01 - 120915: - Parts Carrier Login - Username:'#username' -password:'#password'")
    def "01 - 120915: Validate the Parts Carrier Login functionality"() {
        //1.Launch "Parts Carrier" @URL and click on "Enter" button
        //2.Check for the '@Username' & '@Password' fields
        //3.Check for the "Login" button

        CommonFunctions.login(username,password)

        when:
        at PartsCarrierSitePage
        then:
        assert heading.text() == "LIST OF SITES"

        where: "Getting Test Data from CSV"
        [username, password] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\120915.csv")


    }

}
